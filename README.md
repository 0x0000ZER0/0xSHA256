# 0xSHA256

A `SHA-256` implementation in `C`.

### API

- there is only one function, namely `z0_sha256_hash`:

```c
uint8_t msg[] = "0123456789abcdef"
                "0123456789abcdef"
                "0123456789abcdef"
                "0123456789abcdef"
                "0123456789abcdef";
printf("INFO: %s\n", msg);


uint8_t hash[16];
z0_sha256_hash(msg, sizeof (msg) - 1, hash);

z0_sha256_print(hash);
```
