#ifndef Z0_SHA256
#define Z0_SHA256

#include <stdint.h>
#include <stddef.h>

void
z0_sha256_hash(uint8_t*, uint64_t, uint8_t*);

void
z0_sha256_print(uint8_t *);

#endif
