#include "z0_sha256.h"

#include <stdbool.h>
#include <string.h>

#include <stdio.h>
#ifdef Z0_DEBUG
#include <stdlib.h>

static void
dbg_print_block(uint32_t *b, size_t l) {
        printf("========== Z0 DEBUG ==========\n");
        for (int t = 0; t < l; t += 2)
                printf("%08x %08x\n", b[t], b[t + 1]);
}

static void
dbg_print_u64(uint64_t u) {
        char buff[65];
        itoa(u, buff, 2);
        printf ("Z0 DEBUG: %s\n", buff);
}
#endif

static uint32_t
ch(uint32_t x, uint32_t y, uint32_t z) {
	return (x & y) ^ (~x & z);
}

static uint32_t
maj(uint32_t x, uint32_t y, uint32_t z) {
	return (x & y) ^ (x & z) ^ (y & z);
}

static uint32_t
rotr(uint32_t x, uint32_t n) {
	return (x >> n) | (x << (32 - n));
}

static uint32_t
S0(uint32_t x) {
	return rotr(x, 2) ^ rotr(x, 13) ^ rotr(x, 22);
}

static uint32_t
S1(uint32_t x) {
	return rotr(x, 6) ^ rotr(x, 11) ^ rotr(x, 25);
}

static uint32_t
sig0(uint32_t x) {
	return rotr(x, 7) ^ rotr(x, 18) ^ (x >> 3);
}

static uint32_t
sig1(uint32_t x) {
	return rotr(x, 17) ^ rotr(x, 19) ^ (x >> 10);
}

static bool
is_lend() {
        int t;
        t = 1;

        if (*(char*)&t == 1)
                return true;
        else
                return false;
}

void
z0_sha256_hash(uint8_t *src, uint64_t len, uint8_t *dst) {
        bool lend;
        lend = is_lend();

        static uint32_t K[] = {
        	0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 
        	0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5,
        	0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 
        	0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174,
        	0xE49B69C1, 0xEFBE4786, 0x0FC19DC6, 0x240CA1CC, 
        	0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
        	0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 
        	0xC6E00BF3, 0xD5A79147, 0x06CA6351, 0x14292967,
        	0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 
        	0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85,
        	0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 
        	0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
        	0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 
        	0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3,
        	0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 
        	0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2
        };

        uint32_t H[] = {
        	0x6A09E667, 0xBB67AE85,
        	0x3C6EF372, 0xA54FF53A,
        	0x510E527F, 0x9B05688C,
        	0x1F83D9AB, 0x5BE0CD19
        };

        uint32_t M[16];
        uint32_t W[64];

        uint32_t a;
        uint32_t b;
        uint32_t c;
        uint32_t d;
        uint32_t e;
        uint32_t f;
        uint32_t g;
        uint32_t h;

        uint32_t T1;
        uint32_t T2;

        uint64_t bits;

#ifdef Z0_DEBUG
        dbg_print_u64(len);
#endif
        bool 	 padded;
        bool 	 finished;
        uint64_t off;

        padded   = false;
        finished = false;
        off 	 = len;
        
        while (off > 0 || finished == false) {
                if (off >= 64) { // most common case.
                        memcpy(M, src, 64);
                } else if (off >= 56) {
                        memset(M, 0, sizeof (M)); 
                        memcpy(M, src, off);

                        *((uint8_t*)M + off) = 0x80; //0b 1000 0000
                        padded = true;
                } else {
                        memset(M, 0, sizeof (M)); 
                        memcpy(M, src, off);
                
                        if (padded == false)
                                *((uint8_t*)M + off) = 0x80; //0b 1000 0000
                        bits = __builtin_bswap64(len * 8);
                        memcpy((void*)(M + 14), (void*)&bits, sizeof (bits));

                        finished = true;
                }

                for (int i = 0; i < 16; ++i)
                        W[i] = lend ? __builtin_bswap32(M[i]) : M[i];

#ifdef Z0_DEBUG
                dbg_print_block(W, 16);
#endif

                for (int i = 16; i < 64; ++i)
                        W[i] = sig1(W[i - 2]) + W[i - 7] + sig0(W[i - 15]) + W[i - 16];

                a = H[0];
                b = H[1];
                c = H[2];
                d = H[3];
                e = H[4];
                f = H[5];
                g = H[6];
                h = H[7];

                for (int t = 0; t < 64; ++t) {
                        T1 = h + S1(e) + ch(e, f, g) + K[t] + W[t];
                        T2 = S0(a) + maj(a, b, c);
                        h  = g;
                        g  = f;
                        f  = e;
                        e  = d + T1;
                        d  = c;
                        c  = b;
                        b  = a;
                        a  = T1 + T2;
                }

                H[0] = a + H[0];
                H[1] = b + H[1];
                H[2] = c + H[2];
                H[3] = d + H[3];
                H[4] = e + H[4];
                H[5] = f + H[5];
                H[6] = g + H[6];
                H[7] = h + H[7];

                if (off >= 64) { // most common case.
                        off -= 64;
                        src += 64;
                } else {
                        off = 0;
                }
        }
            
        memcpy(dst, H, sizeof (H));
}

void
z0_sha256_print(uint8_t hash[32]) {
	printf("Z0 INFO: ");

	for (int i = 0; i < 8; ++i) {
		uint32_t dig;
		dig = *(uint32_t*)(hash + i * 4);

		printf("%08X", dig);
	}

	printf("\n");
}
